# NSRLFile Legacy RDSv2 Format Generator Script #

## Description ##

The NIST Information Technology Laboratory distributes the National Software Reference Library (NSRL), which contains unique hashes of "known-good" files. These hashes are used by many forensic software tools and other scanning utilities to find files of interest by ignoring/filtering these "irrelevant" files.

For many years these NSRL files have been released as large text files in comma-separated value (CSV) format, otherwise known as  the "RDSv2 text file format" or an "NSRLFile."  However, as of early 2023, NIST has transitioned away from this format in favor of the newer RDSv3 format, which relies on a more robust SQLite database.

While NIST gave a lengthy transition period for the industry to update to the new v3 format, many software tools still rely on the older format.  Therefore, NIST has provided instructions for generating the RDSv2 formatted text files manually using the SQLite database. This requires a number of individual steps, as well as a basic understanding of a Unix-like environment.  This project provides a script and documentation to simplify this process.

## Requirements ##

**Quick summary *(tl;dr)*:**  Any modern GNU/Linux environment can generate these files if they contain the SQLite 3.x client and basic file utilities like "sed." This includes using a Windows operating system with WSL and a GNU/Linux OS installed, such as Ubuntu or Kali Linux. Run the script and follow the prompts.

### Detailed Requirements ###

* This script requires a Unix-like environment, such as your favorite version of GNU/Linux.  If you use a Windows-based system, you can use the Windows Subsystem for Linux (WSL) to install a GNU/Linux distribution of your choosing, which is generally done through the Microsoft App Store.  Further instructions:
    * [https://learn.microsoft.com/en-us/windows/wsl/install](https://https://learn.microsoft.com/en-us/windows/wsl/install) 
* Your Unix-like system must have the SQLite 3.x client tools installed.
* Your Unix-like system needs Bash or a compatible shell installed. Most other modern shells will also work.
* The generated NSRLFile text files are quite large, so ensure you have adequate disk space.  For example, as of early 2023, the "RDS Modern Minimal" NSRLFile is 37.1 GB in size.

## Installation ##

The script(s) in this project are standalone and do not require any special installation beyond the above Requirements.

## Usage ##

1. Download the necessary NSRL RDS 3.xx archive from NIST [here](https://www.nist.gov/itl/ssd/software-quality-group/national-software-reference-library-nsrl/nsrl-download/current-rds).
1. Unzip/extract the NSRL archive. You will only need to access the *.db file.
1. Open a shell/terminal to run the script. Ensure you are using the most recent version.
1. Follow the prompts. Be patient... this process is usually slow even with a powerful system.

## Support ##

Please use the project Issues List to report any bugs or request enhancements.

## Contributing

If you would like to contribute to the project, feel free to create a Pull Request and provide the necessary explanations/documentation with your request.  For detailed submissions, please create a corresponding request on the Issues List.

### License ###

This project is freely available for use and modification under the Apache 2.0 license. For further information, see the project COPYRIGHT documentation.
