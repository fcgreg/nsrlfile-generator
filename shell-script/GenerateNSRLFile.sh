#!/bin/bash
#
# This shell script is designed to be compatible with a modern GNU Bash 
# implementation such as those found on most GNU/Linux systems.  This is 
# because the format of certain commands (such as "sed") can be different 
# across other Unix-based operating systems (such as FreeBSD or macOS), which
# can cause the script to fail.
#
# This script expects the NSRL RDSv3 (3.x) format database has already been 
# extracted to an accessible location.
#
# Hint: Processing will usually be much faster if your RDSv3 database file is 
# on a different drive than your destination file.
# -----------------------------------------------------------------------------
#
# Get the DB path from STDIN
echo "Enter the path and name to the SQLITE DB file: "
read dbName

# Test if DB file exists
if [ ! -f "$dbName" ]; then
	echo "Error: That DB file does not exist! Please check the path and name. Exiting..."
	exit
fi

# Get the path/name of the output file
echo
echo "Enter the path and name of your output NSRLFile: "
read nsrlFile

# Confirm output file does not alread exist
if [ -f "$nsrlFile" ]; then
	echo "Error: The specified output file already exists! Specify a different location. Exiting..."
	exit
fi

echo

# Open the SQLITE DB and create an EXPORT table
echo "Creating an EXPORT table in SQLITE..."
sqlite3 "${dbName}"  <<EOF
DROP TABLE IF EXISTS EXPORT;
CREATE TABLE EXPORT AS SELECT sha1, md5, crc32, file_name, file_size, package_id FROM FILE;
UPDATE EXPORT SET file_name = REPLACE(file_name, '"', '');
.mode csv
.headers off
.output "${nsrlFile}"
SELECT '"' || sha1 || '"', '"' || md5 || '"', '"' || crc32 || '"', '"' || file_name || '"', file_size, 
package_id, '"' || 0 || '"', '"' || '"' FROM EXPORT ORDER BY sha1;
.q
EOF

# Remove extra quotes per NIST processing instructions
echo
echo "Stripping extra quotes..."
sed -i 's/"""/"/g' "${nsrlFile}"

# Manually add the NSRLFile header into the top of the file
echo
echo "Adding header to NSRLFile..."
sed -i '1 i "SHA-1","MD5","CRC32","FileName","FileSize","ProductCode","OpSystemCode","SpecialCode"' "${nsrlFile}"

# Remove all occurences of \r (carriage returns) from the file to ensure Unix 
# compatibility, per NIST instructions
echo
echo "Confirming file is in a Unix-compatible format..."
sed -i -e "s/\r//g" "${nsrlFile}"

echo
echo "Processing complete. Exiting..."
